# GitLab + TriggerMesh Workshop [WIP]

Welcome to the GitLab + Triggermesh workshop! Today we will make it incredibly
easy for you to get started deploying serverless functions to Kubernetes!

## DISCLAIMER

This is still a work in progress workshop. It is currently only meant to
demonstrate compatibility with simple lamda functions deployed with Knative. The
lambda functions themselves will become more fun and intricate, and the
instructions themselves will get more detailed as I iterate.

Work items:
- [ ] figure out account auto-provision
- [ ] figure out whether we are going to use GCP or Triggermesh cloud so I
      can write instructions accordingly. (Need help from seb)
- [ ] Make the functions do fun image stuff as discussed with Priyanka.
- [ ] General clean-up, add photos, prettying up, copy editing, etc.

## Before We Begin

Ensure you have the following tools installed on your machine:

* curl
* git
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [TriggerMesh CLI v0.0.10](https://github.com/triggermesh/tm/releases)

The TriggerMesh CLI will download a binary to your default download location.
We will move it to be in your `PATH` later.

## Login To GitLab

With the credentials provided, please login to your pre-provisioned account
on [GitLab.com](https://gitlab.com/users/sign_in).

**TODO:** figure out account auto-provision
**TODO:** add screenshot

## Import This Repo

Once logged in, click the green `New Project` button at the top right-hand
corner of the page.

**TODO:** picture of the above

Navigate to the `Import Repo` tab and select `Repo by URL`.

**TODO:** picture of the above.

Fill out the page with the following information:

* Git Repository URL: `https://gitlab.com/eggshell/triggermesh-workshop`
* Leave `Mirror Repository` unchecked
* Project Name: `workshop`
* Project Slug: `workshop`
* Visibility Level: `Public`

Click the green `Create Project` button at the bottom of the page.

**TODO:** picture of the above screen filled out correctly.

## Get a Kubernetes Cluster

**TODO:** need to figure out if we are going to supply clusters or if TM will
provide users access to TM cloud. If we use TM cloud, we could probably skip
this step.

## Install Knative on Kubernetes Cluster

**TODO:** need to figure out if we are going to supply clusters or if TM will
provide users access to TM cloud. If we use TM cloud, we could probably skip
this step.

## Configure Knative domain

**TODO:** need to figure out if we are going to supply clusters or if TM will
provide users access to TM cloud. If we use TM cloud, we could probably skip
this step.

## Install knative-local-registry

**TODO:** need to figure out if we are going to supply clusters or if TM will
provide users access to TM cloud. If we use TM cloud, we could probably skip
this step.

ref: https://github.com/triggermesh/knative-local-registry

## Clone Your Repo

Navigate to your project's main page by clicking on it, then click the blue
`Clone` button and clone your repo. Here is an example:

```shell
git clone https://gitlab.com/<YOUR_GITLAB_USERNAME>/workshop
```

## Deploy a Lambda Function

Navigate to your project's `lambda` directory:

```shell
cd workshop/lambda
```

Move the `tm` binary to this directory:

```shell
mv ~/Downloads/tm[_osx] ./tm
```

Deploy a lambda function to Kubernetes:

```shell
./tm deploy service go-lambda -f . --build-template https://raw.githubusercontent.com/triggermesh/knative-lambda-runtime/master/go-1.x/buildtemplate.yaml --wait
```

This will take a little while to run, but the finished resulting output will
look like:

```shell
Creating go-lambda function
Uploading sources to go-lambda-00003-pod-43acf5
Waiting for go-lambda ready state
Service go-lambda URL: http://go-lambda.default.tm-workshop.eggshell.me
```

The final line indicating `Service go-lambda URL` is important, as that is the
endpoint for your newly-deployed lambda function.

**TODO:** just add `tm` to their path in a non-destructive way.

## Validate Lambda Function

Run the following command to trigger your serverless function:

```shell
curl http://YOUR_SERVICE_URL --data '{"Name": "GitLab"}'
```

The resulting output will look like this:

```shell
"Hello GitLab!"
```

This means that your lambda function works and responds to requests. Hooray!
